FROM kalilinux/kali-rolling:latest

RUN apt update -o Acquire::https::Verify-Peer=false
RUN apt install ca-certificates -y -o Acquire::https::Verify-Peer=false
RUN apt install -y adduser vim sudo
RUN addgroup kali --gid 1000
RUN useradd -d /home/kali -g 1000 -m -u 1000 kali
RUN sed -i "s%kali:\!:%kali:\$y\$j9T\$cxmmDSKZ6OFsOZgWzgJlj0\$yIIY2Boqso2QJO711b33zN0JNtLl/4qFTzKdh4tTQA4:%" /etc/shadow
RUN echo "kali ALL=(ALL:ALL) ALL" > /etc/sudoers.d/kali
RUN chmod +r /var/cache/apt/archives/lock; \
chmod +r /var/cache/apt/archives/partial; \
chmod +r /var/cache/debconf/passwords.dat; \
chmod +x /var/cache/ldconfig; \
chmod -R +r /var/cache/ldconfig; \
chmod +r /var/lib/dpkg/lock; \
chmod +r /var/lib/dpkg/lock-frontend; \
chmod +r /var/lib/dpkg/triggers/Lock; \
chmod +r /var/lib/apt/lists/lock; \
chmod +rx /var/lib/apt/lists/partial; \
chmod -R +r /var/log/apt; \
chmod +r /var/log/btmp
RUN touch /root/.hushlogin; \
touch /home/kali/.hushlogin